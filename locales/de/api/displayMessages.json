{
	"httpMethodNotAllowed": "Die HTTP-Methode \"{{method}}\" ist für diesen Endpunkt nicht zulässig",
	"endpointNotFound": "Endpunkt nicht gefunden",
	"errorOccured": "Ein Fehler ist aufgetreten",
	"missingParameter": "Fehlender {{parameter}}",
	"invalidParameter": "Ungültiger {{parameter}}",
	"invalidParameterType": "{{parameter}} muss ein {{type}} sein",
	"parameterTooLong": "{{parameter}} kann nicht länger als {{maxLen}} Zeichen sein",
	"parameterTooShort": "{{parameter}} muss mindestens {{minLen}} Zeichen lang sein",
	"parameterNotBetween": "{{parameter}} muss zwischen {{min}} und {{max}} liegen",
	"discordAccountNotLinked": "Ein Discord-Konto muss verknüpft sein",
	"notLoggedIn": "Sie müssen eingeloggt sein, um diesen Endpunkt zu verwenden",
	"alreadyLoggedIn": "Sie sind bereits eingeloggt",
	"ungültigeUsernameZeichen": "Der Benutzername darf nur englische alphanumerische Zeichen enthalten",
	"usernameTaken": "Benutzername bereits vergeben",
	"passwordMismatch": "Passwörter stimmen nicht überein",
	"changePasswordSuccess": "Passwort erfolgreich geändert",
	"resourceNotFound": "Eine {{resource}} mit dieser {{property}} konnte nicht gefunden werden",
	"resourceExists": "Eine {{resource}} mit dieser {{property}} existiert bereits",
	"premiumFeature": "Sie benötigen ein erweitertes Abonnement, um {{feature}} zu nutzen",
	"userNotFound": "Dieser Benutzer konnte nicht gefunden werden",
	"fileTooLarge": "Datei zu groß. Maximale Dateigröße ist {{size}}",
	"invalidFileFormat": "Ungültiges Dateiformat",
	"bodyTooLarge": "Der angegebene Textkörper ist zu groß",
	"bodyMalformed": "Der angegebene Textkörper ist fehlerhaft",
	"authenticationApi": {
		"accountBanned": "Ihr Konto wurde gesperrt. Grund: {{reason}}",
		"invalidForgotPasswordToken": "Passwort vergessen Token war entweder ungültig oder ist abgelaufen",
		"passwordResetLinkSent": "Es wurde versucht, einen Link zum Zurücksetzen des Passworts an Ihre Discord-DMs zu senden. Dieser Link wird in 24 Stunden ablaufen.",
		"suspectedMultiAccounting": "Es sieht so aus, als ob du mehere Konten betreiben könntest. Please contact upload.systems support if you think this is an error.",
		"noReasonProvided": "Kein Grund angegeben",
		"invalidInviteCode": "Der Einladungscode existiert nicht oder wurde bereits verwendet",
		"refreshTokenMismatch": "Der Refresh-Token stimmt nicht mit dem angegebenen Token überein",
		"passwordResetRequired": "Sie müssen Ihr Passwort zurücksetzen",
		"notBetaUser": "Sie müssen ein Beta-Benutzer sein, um auf diese Website zugreifen zu können",
		"mfaCodeRequired": "MFA-Code ist erforderlich",
		"failedCaptcha": "Captcha fehlgeschlagen, bitte versuchen Sie es erneut"
	},
	"collectionsApi": {
		"invalidPublicityType": "Publicity muss \"public\" oder \"private\" sein",
		"collectionNotEmpty": "Die Sammlung muss vor dem Löschen leer sein"
	},
	"discordApi": {
		"accountTooYoung": "Ihr Discord-Konto ist nicht alt genug, um verlinkt zu werden",
		"accountNotFound": "Konnte nicht über Discord authentifiziert werden. Es ist kein Konto mit diesem Discord-Konto verknüpft."
	},
	"domainGroupsApi": {
		"usersMustBeArray": "Benutzer müssen ein Array von UUIDs sein",
		"noValidUsers": "Benutzer müssen mindestens 1 gültige Benutzer-UUID enthalten",
		"invalidUserIds": "Ungültige Benutzer-ID(s) wurden angegeben",
		"groupInUse": "Diese Zugriffsgruppe wird derzeit von einer Domäne verwendet, bitte setzen Sie diese auf eine andere Gruppe, bevor Sie sie löschen"
	},
	"domainsApi": {
		"invalidTld": "Wir akzeptieren keine Domains mit dieser TLD, Entschuldigung",
		"deletedDomain": "Domain wurde Erfolgreich gelöscht"
	},
	"imagesApi": {
		"futureTimestamp": "Ausgelaufen am muss ein Zeitstempel in der Zukunft sein",
		"unauthenticated": "Sie müssen authentifiziert sein, um diesen Endpunkt zu verwenden",
		"invalidUploadKey": "Ungültiger Upload-Schlüssel. Haben Sie einen Schlüssel generiert?",
		"subscriptionMaxFileSize": "Datei überschreitet die maximale Dateigröße Ihres Abonnements von {{size}}",
		"subscriptionMaxStorage": "Sie haben Ihr Speicherlimit erreicht. Bitte löschen Sie einige Dateien oder erwägen Sie ein Upgrade Ihres Abonnements.",
		"missingUploadPreferences": "Keine Domain gefunden, bitte legen Sie Ihre Upload-Präferenzen auf der Einstellungsseite fest",
		"userBanned": "Sie sind derzeit gesperrt, bitte kontaktieren upload.systems Sie den Support, wenn Sie glauben, dass dies ein Fehler ist",
		"unrecognisedIp": "Unerkannte IP-Adresse. Bitte loggen Sie sich auf der Website ein und versuchen Sie es erneut.",
		"missingExtension": "Die Datei muss eine Erweiterung haben",
		"premiumFileType": "Sie benötigen ein erweitertes Abonnement, um diesen Dateityp hochzuladen",
		"spamUploadBan": "Sie wurden gesperrt, weil Sie zu schnell Bilder hochgeladen haben, wenden upload.systems Sie sich an den Support, um Einspruch zu erheben",
		"noDeletePermissions": "Sie haben nicht die Erlaubnis, die Bilder eines anderen Benutzers zu löschen",
		"wipeInProgress": "Ihre Bilder werden bereits gelöscht ({{percentage}}% abgeschlossen)",
		"noImagesInCollection": "Keine Bilder in der angegebenen Sammlung",
		"noValidImageIds": "Keine gültigen Bild-IDs angegeben",
		"noImagesTo": "Sie haben keine Bilder für {{action}}",
		"archiveInProgress": "Sie haben bereits ein Archiv in Bearbeitung",
		"archiveAlreadyRequested": "Sie haben innerhalb der letzten 7 Tage bereits ein Archiv angefordert",
		"reportOwnImage": "Sie können Ihre eigenen Bilder nicht melden",
		"imageAlreadyReported": "Sie haben dieses Bild bereits gemeldet",
		"cannotSetFileAsShowcase": "Sie können nur Bilder oder Videos als Showcase einstellen"
	},
	"mailAccountsApi": {
		"invalidDomain": "Ungültige Domäne in der E-Mail",
		"maxAccounts": "Sie haben die maximale Anzahl von E-Mail-Konten erreicht, die Sie auf einmal haben können",
		"maxAliases": "Sie haben die maximale Anzahl von E-Mail-Aliasen erreicht, die Sie auf einmal haben können",
		"emailInUse": "E-Mail-Adresse bereits in Gebrauch",
		"aliasExists": "Ein Alias mit dieser E-Mail-Adresse existiert bereits",
		"invalidCharacters": "Sie haben ungültige Zeichen in Ihrer E-Mail-Adresse"
	},
	"notificationsApi": {
		"alreadyMarkedAsRead": "Diese Benachrichtigung ist bereits als gelesen markiert"
	},
	"pastesApi": {
		"unsupportedLang": "Die angegebene Sprache für die Syntaxhervorhebung wird nicht unterstützt",
		"unsupportedType": "Der angegebene Einfügetyp wird nicht unterstützt",
		"privateAndPasswordProtected": "Eine Einfügung kann nicht gleichzeitig privat und passwortgeschützt sein",
		"successfullyWiped": "Erfolgreich gelöschte {{count}} Pasten",
		"passwordRequired": "Diese Paste erfordert ein Passwort zum Entschlüsseln",
		"incorrectPassword": "Falsches Passwort",
		"notOwnPaste": "Sie können nur Ihre eigenen Einfügungen löschen"
	},
	"shortenUrlApi": {
		"invalidProtocol": "Ungültiges URL-Protokoll. Muss HTTP oder HTTPS verwenden",
		"invalidIDCharacters": "ID darf nur englische und numerische Zeichen enthalten",
		"selfRedirect": "URL kann nicht auf sich selbst umleiten",
		"successfullyWiped": "Erfolgreich gelöschte {{count}} verkürzte URLs",
		"notOwnShortenedUrl": "Sie können nur Ihre eigenen verkürzten URLs löschen"
	},
	"subscriptionsApi": {
		"notSelected": "Sie haben keine Abonnementgruppe ausgewählt",
		"activeSubscription": "Sie haben bereits ein aktives Abonnement",
		"groupMismatch": "Der angegebene Benutzer hat bereits ein Abonnement für den Tarif {{groupName}}, daher kann dieser Tarif nicht verschenkt werden",
		"selfGift": "Sie können sich nicht selbst ein Abonnement schenken, fügen Sie stattdessen Monate auf Ihrer Abonnementverwaltungsseite hinzu",
		"userNotGiftable": "Sie können diesem Benutzer im Moment kein Abonnement schenken, bitten sie ihn, sich zuerst anzumelden",
		"noSubscriptionAddMonths": "Sie haben kein aktives Abonnement, zu dem Sie Monate hinzufügen können",
		"noSubscriptionCancel": "Sie haben kein aktives Abonnement, das Sie kündigen können",
		"noSubscriptionResume": "Sie haben kein aktives Abonnement, das Sie fortsetzen können",
		"pendingPlanChange": "Sie haben bereits eine anstehende Planänderung",
		"noDefaultPaymentMethod": "Sie müssen mindestens eine Zahlungsmethode angegeben haben, um dieses Abonnement fortzusetzen",
		"cannotPreviewFree": "Sie können einen Wechsel zu einem kostenlosen Tarif nicht vorprüfen, kündigen Sie stattdessen Ihr Abonnement",
		"cannotPreviewNoSubscription": "Sie können eine Änderung ohne aktives Abonnement nicht in der Vorschau anzeigen",
		"cannotPreviewSamePlan": "Sie können eine Änderung an Ihrem aktuellen Plan nicht in der Vorschau anzeigen",
		"cannotChangeAddedMonths": "Sie können jetzt nicht den Tarif wechseln, da Sie Ihrem bestehenden Abonnement Monate hinzugefügt haben. Sie können Ihr Abonnement nach {{date}} ändern.",
		"cannotChangeGifted": "Sie können im Moment nicht wechseln, da Sie ein geschenktes Abonnement verwenden. Sie können Ihren Plan nach {{date}} ändern."
	},
	"themesApi": {
		"noDeletePermissions": "Sie haben keine Berechtigung, dieses Thema zu löschen"
	},
	"userApi": {
		"privateProfile": "Das Profil dieses Benutzers ist privat",
		"noValidProperties": "Es wurden keine gültigen Eigenschaften angegeben",
		"multiLevelSubdomains": "Mehrstufige Subdomains werden nicht unterstützt. Verwenden Sie - anstelle von .",
		"subdomainsNotAllowed": "Subdomains sind auf dieser Domain nicht erlaubt",
		"maxDomainCount": "Sie können nur bis zu {{count}} Domains auf einmal auswählen",
		"minDomainCount": "Sie müssen mindestens 2 Domains auf einmal ausgewählt haben",
		"invalidDomain": "{{domain}} ist keine gültige Domain",
		"invalidSubdomainDomain": "Subdomains sind auf {{domain}} nicht erlaubt",
		"autoChanceZero": "Die angegebenen Werte führen dazu, dass die 'Auto'-Chance 0% beträgt",
		"randomChanceNot100": "Die Werte für die zufällige Domain-Chance müssen sich zu 100 addieren",
		"invalidUrl": "{{property}} muss eine gültige HTTP- oder HTTPS-URL sein",
		"invalidEmbedFields": "Es können nicht alle Einbettungsfelder leer sein. Bitte füllen Sie einige Felder aus, oder deaktivieren Sie Discord Embeds",
		"noAuthorOrTitle": "Damit Discord korrekt eingebettet werden kann, ist ein Autor oder Titel erforderlich",
		"invalidTimeformat": "Zeitformat muss entweder 12h oder 24h sein",
		"cropAreaExceedsSize": "Der Beschnittbereich darf die Bildgröße nicht überschreiten",
		"usernameChangeUnavailable": "Sie haben Ihren Benutzernamen in den letzten 7 Tagen bereits geändert",
		"unsupportedLanguage": "Nicht unterstützte Sprache angegeben",
		"invalidConfigType": "Ungültiger Konfigurationstyp angegeben",
		"unprocessableGif": "Wir waren nicht in der Lage, dieses GIF zu verarbeiten"
	},
	"userApis": {
		"MFAApi": {
			"mfaNotInitalised": "MFA wurde nicht initialisiert",
			"mfaNotEnabled": "Sie haben MFA nicht aktiviert"
		},
		"paymentApi": {
			"atLeastOnePaymentMethodRequired": "Sie müssen mindestens eine Zahlungsmethode mit einem aktiven Abonnement haben",
			"defaultPaymentMethod": "Sie können keine Zahlungsmethode löschen, die als Standard markiert ist",
			"amountDecimalPlaces": "Der Betrag darf nicht mehr als 2 Dezimalstellen haben",
			"amountTooSmall": "Betrag kann nicht kleiner sein als {{ammount}}",
			"amountTooLarge": "Betrag kann nicht größer sein als {{ammount}}"
		},
		"uploadPresetsApi": {
			"missingParameters": "Es fehlen Parameter in Ihrer Anfrage",
			"maxPresets": "Sie können nur bis zu 10 Upload-Voreinstellungen gleichzeitig haben"
		}
	}
}
